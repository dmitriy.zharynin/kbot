/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package main

import "gitlab.com/dmitriy.zharynin/kbot/cmd"

func main() {
	cmd.Execute()
}
